module movement

import java.lang.Math.*
import it.unibo.alchemist.custom.CoordinationUtil.*

// Euclidean norm of a two-dimensional vector.
public def norm(v) {
    sqrt(pow(v.get(0), 2) + pow(v.get(1), 2))
}

// Euclidean norm elevated to the p-th power.
public def powNorm(v, p) {
    pow(pow(v.get(0), 2) + pow(v.get(1), 2), p/2)
}

// Crop a coordinate to fit into a rectangle.
public def cropRectangle(x, low, hi) {
    [min(max(x.get(0), low.get(0)), hi.get(0)), 
     min(max(x.get(1), low.get(1)), hi.get(1))]
}

// Project x to fit within circle of center c and radius r. 
public def cropCircle(x, c, r) {
	let d = x - c;
    if (pow(d.get(0), 2) + pow(d.get(1), 2) < pow(r,2)) { x } else {
		c + (d / norm(d) * r)
    }
}

// Uniform random vector of norm up to r.
public def randVector(r) {
    let theta = 2*PI*self.nextRandomDouble();
    sqrt(self.nextRandomDouble()) * r * [cos(theta),sin(theta)]
}

// Random vector within the rectangle bounded by points "lo" and "hi".
public def randRect(lo, hi) {
    [lo.get(0) + (hi.get(0)-lo.get(0))*self.nextRandomDouble(),
    	 lo.get(1) + (hi.get(1)-lo.get(1))*self.nextRandomDouble()]
}

// Returns a goal by applying function "goal", and regenerates it whenever
// the distance from the current goal drops below "mindist".
public def ifClose(goal, dist) {
    rep (x <- goal.apply()) {
        if (computeDistance(self, x) <= dist)
        { goal.apply() } else { x }
    }
}

// Walk to random targets within a rectangle of given size, changing targets within reach.
public def rectangleWalk(rectSize, reach) {
	let target = if (env.has('dspace')) {
		let dspace = env.get('dspace');
		if (dspace == 0) {
			cropRectangle(self.getCoordinates(), [0,0], rectSize)
		} else {
			ifClose(() -> {cropRectangle(self.getCoordinates()+randVector(dspace), [0,0], rectSize)}, reach)
		}
	} else {randRect([0,0], rectSize)};
	env.put('target', target)
}
