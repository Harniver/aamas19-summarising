#/usr/bin/env pypy

from random import *
from math import *
from sys import *

def dist(p):
    return sqrt(p[0]**2 + p[1]**2)

def weight(p):
    return p[1] * (1-dist(p)) # p[1]

# random point in a circle
def circlepoint(r, p = [0,0]):
    a = 2*pi*random()
    r *= random() ** 0.5
    return [p[0]+r*cos(a), p[1]+r*sin(a)]

# probability that a point at distance d from origin stays within unit circle after moving at most r
def survival(d, r):
    if d+r <= 1:
        return 1
    p = (d+r+1)/2
    alpha = acos((d**2+1-r**2)/(2*d))
    beta  = acos((d**2+r**2-1)/(2*d*r))
    area  = sqrt(p*(p-1)*(p-d)*(p-r))
    return ((alpha - 2*area)/r**2 + beta)/pi

# given n devices in 2R^2 space, min distance by threshold
def threshold_to_dist(n):
    points = [(2*random()-1, random()) for _ in xrange(n)]
    points = [(weight(p), p[1]) for p in points if dist(p) < 1]
    points.sort()
    wt = sum([p[0] for p in points]) / 2 / len(points) # 1
    mp = max([p[1] for p in points])
    points = [[p[0] / wt, p[1] / mp] for p in points]
    for i in reversed(xrange(len(points)-1)):
        points[i][1] = min(points[i][1], points[i+1][1])
    return map(tuple, points)

def polynomial(x, coeff):
    return sum([coeff[i]*(x**i) for i in xrange(len(coeff))])

# failure probability given probabilities of having k neighbours
def failure(prob, hops):
    np = 0
    for _ in xrange(hops):
        pf = np
        np = polynomial(pf, prob)
    return np

print 'import "plot.asy" as plot;'
print 'unitsize(1cm);'
print 'plot.OFFS = (1.5,1.5);'
print 'plot.COLS = 3;'
print 'plot.ROWS = 4;'
seed(42)
RUNS = 10000
TFAC = 4.0 # 24.0

failure_probs = []
failure_caps  = []
for rho in [10,20,50,100]:
    data = [threshold_to_dist(rho) for _ in xrange(RUNS)]
    avg = [0 for _ in xrange(25)]
    probs = [[0 for _ in xrange(rho+1)] for _ in xrange(25)]
    for d in data:
        n = 0
        for t in xrange(25):
            while n < len(d) and TFAC*d[n][0] < t:
                n += 1
            probs[t][len(d)-n] += 1.0/RUNS
            avg[t] += float(len(d)-n) / RUNS
    probs = [(i/TFAC, failure(probs[i], 1000)) for i in xrange(25)]
    avg   = [(i/TFAC, avg[i]) for i in xrange(25)]
    failure_caps.append('"$\\rho = %d$"' % (rho/2))
    failure_probs.append(probs)
    print '\nplot.put(plot.plot("threshold-failure-%d", "failure rate $\\rho = %d$", "threshold", "failure", new pair[][] %s));' % (rho/2, rho/2, repr([probs]).replace('[','{').replace(']','}'))
    print '\nplot.put(plot.plot("threshold-neighbours-%d", "connected neighbours $\\rho = %d$", "threshold", "neighbours", new pair[][] %s));' % (rho/2, rho/2, repr([avg]).replace('[','{').replace(']','}'))

    data = sum(data[:RUNS/10], [])
    data.sort()
    ex = sum([d[0] for d in data]) / len(data)
    ey = sum([d[1] for d in data]) / len(data)
    vx = sum([(d[0] - ex)**2 for d in data])
    cv = sum([(d[0] - ex)*(d[1] - ey) for d in data])
    b  = cv / vx
    a  = ey - b*ex
    print >> stderr, '\nlinear regression (rho = %d): %.4f x + %.4f' % (rho/2, b, a)
    speed = []
    for i in xrange(100):
        d = data[i*len(data)/100:(i+1)*len(data)/100]
        x = sum(dd[0] for dd in d) / len(d)
        y = sum(dd[1] for dd in d) / len(d)
        speed.append((x,y))
    print '\nplot.put(plot.plot("threshold-speed-%d", "information speed $\\rho = %d$", "threshold", "speed", new pair[][] %s));' % (rho/2, rho/2, repr([[(0,a),(24/TFAC,a+b*24/TFAC)], speed]).replace('[','{').replace(']','}'))

print '\nplot.put(plot.plot("threshold-failure", "", "threshold", "failure", new string[] {%s}, new pair[][] %s));' % (', '.join(failure_caps), repr(failure_probs).replace('[','{').replace(']','}'))

print '\nshipout("threshold");'
